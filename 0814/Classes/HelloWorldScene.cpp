#include "HelloWorldScene.h"
#include "To_LeadScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "Tutorial.h"



USING_NS_CC;
using namespace cocostudio::timeline;
using namespace cocos2d::ui;
#define DIRECTOR(SCENE) Director::getInstance()->replaceScene(##SCENE)


Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
    scene->addChild(layer);
	layer->HelloMain();
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() ) return false;
    return true;
}
//메인
void HelloWorld::HelloMain(){

	char* text[3] = { "Start", "To lead", "end" };
	Scene* Scene[2] = {
		Tutorial::TutorialScene(),
		Tolead::leadScene()
	};
	
	HelloWorld::ButtonGenerator(Vec2(480, 300 - 0 * 80), text[0], [=](Ref* rf)->void{DIRECTOR(Tutorial::TutorialScene()); });
	HelloWorld::ButtonGenerator(Vec2(480, 300 - 1 * 80), text[1], [=](Ref* rf)->void{DIRECTOR(Tolead::leadScene()); });
	HelloWorld::ButtonGenerator(Vec2(480, 300 - 2 * 80), text[2], [=](Ref* rf)->void{ });
}
//버튼 만들기
void HelloWorld::ButtonGenerator(Vec2 vec, char* text, const Button::ccWidgetClickCallback& Scene){
	Button* btn = Button::create("res/bin.png");
	btn->setTitleText(text);
	btn->setTitleFontSize(20);
	btn->setTitleColor(Color3B::BLACK);
	btn->addClickEventListener(Scene);
	btn->setPosition(vec);
	addChild(btn);
}
