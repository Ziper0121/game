#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
	CREATE_FUNC(HelloWorld);

	void HelloMain();
	void ButtonGenerator(cocos2d::Vec2, char*, const cocos2d::ui::Button::ccWidgetClickCallback& Scene);
};

#endif // __HELLOWORLD_SCENE_H__
