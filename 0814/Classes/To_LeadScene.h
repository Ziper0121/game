#ifndef __TO_LEAD_SCENE_H__
#define __TO_LEAD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class Tolead : public cocos2d::Layer
{
public:
	static cocos2d::Scene* leadScene();
	virtual bool init();
	CREATE_FUNC(Tolead);

	void To_LeadMain();
	void ButtonGenerator(cocos2d::Vec2, char*, const cocos2d::ui::Button::ccWidgetClickCallback& Scene);
};

#endif