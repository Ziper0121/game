#include "To_LeadScene.h"
#include "HelloWorldScene.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;
using namespace cocos2d;
#define DIRECTOR(SCENE) Director::getInstance()->replaceScene(##SCENE)

Scene* Tolead::leadScene(){
	Scene* scen = Scene::create();
	Tolead* lead = Tolead::create();
	scen->addChild(lead);
	lead->To_LeadMain();
	return(scen);
}

bool Tolead::init(){
	if (!Layer::init()) return false;
	return true;
}
//메인
void Tolead::To_LeadMain(){
	Tolead::ButtonGenerator(Vec2(900, 60),
		"Back", [=](Ref* rf)->void{ DIRECTOR(HelloWorld::createScene()); });
}
//버튼 만들기
void Tolead::ButtonGenerator(Vec2 vec, char* text,const Button::ccWidgetClickCallback& Scene){
	Button* btn = Button::create("res/bin.png");
	btn->setTitleText(text);
	btn->setTitleFontSize(20);
	btn->setTitleColor(Color3B::BLACK);
	btn->addClickEventListener(Scene);
	btn->setPosition(vec);
	addChild(btn);
}
