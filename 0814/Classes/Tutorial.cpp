#include "Tutorial.h"
#include "HelloWorldScene.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;
using namespace cocos2d;
#define DIRECTOR(SCENE) Director::getInstance()->replaceScene(##SCENE)

Scene* Tutorial::TutorialScene(){
	Scene* scene = Scene::create();
	Tutorial* layer = Tutorial::create();
	scene->addChild(layer);
	layer->TutorialMain();
	return scene;
}
bool Tutorial::init(){
	if (!Layer::init())return false;

	return true;
}
//메인
void Tutorial::TutorialMain(){
	Tutorial::ButtonGenerator(Vec2(900,60),
		"Back", [=](Ref* rf)->void{ DIRECTOR(HelloWorld::createScene()); });
}
//버튼 만들기
void Tutorial::ButtonGenerator(Vec2 vec, char* text, const Button::ccWidgetClickCallback& Scene){
	Button* btn = Button::create("res/bin.png");
	btn->setTitleText(text);
	btn->setTitleFontSize(20);
	btn->setTitleColor(Color3B::BLACK);
	btn->addClickEventListener(Scene);
	btn->setPosition(vec);
	addChild(btn);
}
void Tutorial::onToutchEvent(cocos2d::EventKeyboard::KeyCode* tch, cocos2d::Event* et){
}
void Tutorial::offToutchEvent(cocos2d::EventKeyboard::KeyCode* tch, cocos2d::Event* et){
	
}
void Tutorial::setboolKeyCode(int Code, bool bol){
	KeyCode[Code] = bol;
}
void Tutorial::getboolKeyCode(int Code){
	KeyCode[Code];
}