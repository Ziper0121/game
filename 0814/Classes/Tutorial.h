#ifndef __TUTORIAL_H__
#define __TUTORIAL_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class Tutorial : public cocos2d::Layer{
public:
	static cocos2d::Scene* TutorialScene();
	virtual bool init();
	CREATE_FUNC(Tutorial);

	void setboolKeyCode(int, bool);
	void getboolKeyCode(int);

	void onToutchEvent(cocos2d::EventKeyboard::KeyCode*, cocos2d::Event*);
	void offToutchEvent(cocos2d::EventKeyboard::KeyCode*, cocos2d::Event*);

	void TutorialMain();
	void ButtonGenerator(cocos2d::Vec2 vec, char* text, const cocos2d::ui::Button::ccWidgetClickCallback& Scene);
private:
	bool KeyCode[128];
};


#endif